#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

VERIFY=1
while [[ "$MYSQL_PW" != "$VERIFY" ]]; do 
  read -sp "Mot de passe MYSQL: " MYSQL_PW
	printf '\n'
	read -sp "Confirmation: " VERIFY
	printf '\n'
	if [[ "$MYSQL_PW" != "$VERIFY" ]]; then
		sleep 1
		echo "Les mots de passe ne correspondent pas"
	fi
done
read -p "Nom du serveur ? " ZABBIX_HOST
VERIFY=1
while [[ "$ZABBIX_PW" != "$VERIFY" ]]; do
  read -p "Utilisateur Zabbix: " ZABBIX_USER
  read -sp "Mot de passe ${ZABBIX_USERNAME}: " ZABBIX_PW
	printf '\n'
	read -sp "Confirmation: " VERIFY
	printf '\n'
	if [[ "$ZABBIX_PW" != "$VERIFY" ]]; then
		sleep 1
		echo "Les mots de passe ne correspondent pas"
	fi
done
apt-get install -y git curl wget 
DISTRO=$(grep -m2 'ID' /etc/os-release | tail -n1 | cut -f2 -d=)
CODENAME=$(grep 'CODENAME' /etc/os-release | cut -f2 -d=)

# Install
if [[ $DISTRO == 'debian' ]] && [[ $CODENAME == 'bullseye' ]]; then
  wget 'https://repo.zabbix.com/zabbix/6.2/debian/pool/main/z/zabbix-release/zabbix-release_6.2-1+debian11_all.deb'
  dpkg -i 'zabbix-release_6.2-1+debian11_all.deb'
  apt-get update
  apt-get install -y mariadb-server zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-sql-scripts zabbix-agent
fi
if [[ $DISTRO == 'ubuntu' ]] && [[ $CODENAME == 'jammy' ]]; then
	wget 'https://repo.zabbix.com/zabbix/6.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.2-1+ubuntu22.04_all.deb'
  dpkg -i 'zabbix-release_6.2-1+ubuntu22.04_all.deb'
	apt-get update
	apt-get install -y mariadb-server zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-sql-scripts zabbix-agent
fi

# Install Zabbix-CLI
curl -s https://api.github.com/repos/unioslo/zabbix-cli/releases/latest \
| grep "browser_download_url.*deb" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget --show-progress -qi -
dpkg -i zabbix-cli*.deb

# Configure database
mysql << MYSQL
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
CREATE DATABASE zabbix character set utf8mb4 collate utf8mb4_bin;
CREATE USER zabbix@localhost IDENTIFIED BY '$MYSQL_PW';
GRANT ALL PRIVILEGES ON zabbix.* TO zabbix@localhost;
FLUSH PRIVILEGES;
MYSQL
echo "Installing default scheme and data..."
zcat /usr/share/doc/zabbix-sql-scripts/mysql/server.sql.gz | mysql -uzabbix -p$MYSQL_PW zabbix

# Run
systemctl restart zabbix-server zabbix-agent apache2
systemctl enable zabbix-server zabbix-agent apache2

# Setup
ZABBIX_CONF=/etc/zabbix/web/zabbix.conf.php
install -o www-data -g root -m 600 src/zabbix.conf.php $ZABBIX_CONF
sed -i "s/MYSQL_PW/${MYSQL_PW}/g" $ZABBIX_CONF
sed -i "s/ZABBIX_HOST/${ZABBIX_HOST}/g" $ZABBIX_CONF

# Adduser and disable admin
zabbix-cli-init -z http://127.0.0.1/zabbix
ZABBIX_USERNAME=Admin ZABBIX_PASSWORD=zabbix zabbix-cli -C \
	"create_usergroup 'All-users' '0' '0'"
ZABBIX_USERNAME=Admin ZABBIX_PASSWORD=zabbix zabbix-cli -C \
	"create_user \"$ZABBIX_USER\" \"$ZABBIX_USER\" \"$ZABBIX_USER\" \"$ZABBIX_PW\" '3' '0' '86400' ''"
ZABBIX_USERNAME=$ZABBIX_USER ZABBIX_PASSWORD=$ZABBIX_PW zabbix-cli -C \
	"add_user_to_usergroup 'Admin' 'Disabled'"

IP=$(ip a | grep -A2 2: | grep inet | cut -f6 -d' ' | cut -f1 -d/)
echo "Installation terminée"
echo "URL du serveur : http://$IP/zabbix"
